document.addEventListener('DOMContentLoaded', function()
{
  var connectGdriveBtn = document.getElementById('connect-gdrive');
  var connectBtn = document.getElementById('connect');
  var disconnectBtn = document.getElementById('disconnect');
  var code_thread = null;

  if (remote.getGlobal('data').dropbox_connected)
  {
    document.getElementById('status').textContent = 'Connected';
    document.getElementById('status').style.color = 'green';
    document.getElementById('acc_name').textContent = JSON.parse(remote.getGlobal('data').dropbox_acc_info).name.display_name;
    document.getElementById('acc_email').textContent = JSON.parse(remote.getGlobal('data').dropbox_acc_info).email;
    document.getElementById('acc_country').textContent = JSON.parse(remote.getGlobal('data').dropbox_acc_info).country;
    connectBtn.style.display = 'none';
    disconnectBtn.style.display = 'inline-block';
  }
  else
  {
    disconnectBtn.style.display = 'none';
    connectBtn.style.display = 'inline-block';
  }
  connectGdriveBtn.addEventListener('click', (event) =>
  {
    remote.getGlobal('data').gdrive_token = JSON.parse(JSON.parse(require('../neon').gdrivePkceHandshake())).access_token;
    //console.log(require('../neon').getGdriveInfo(remote.getGlobal('data').gdrive_token));
  });
  connectBtn.addEventListener('click', (event) =>
  {
    state = makeid(20);
    shell.openExternal('https://www.dropbox.com/1/oauth2/authorize?client_id=2ezekn40iurcs2i&response_type=code&redirect_uri=http://localhost:65000/callback&state=' + state);
    try
    {
      if (code_thread === null)
        code_thread = new Promise(resolve => {
          require('../neon').waitUntilDropboxRedirect((err, value) => {
            remote.getGlobal('data').dropbox_code = value.split('&')[1].replace('code=', '').replace(' ', '');
            resolve();
          });
        });
      code_thread.then(() => {
        code_thread = null;
        var code = remote.getGlobal('data').dropbox_code;
        token_reply = JSON.parse(require('../neon').getDropboxToken(String(code)));
        if (token_reply.access_token != '')
        {
          remote.getGlobal('data').user_config['dropbox']['token'] = token_reply.access_token;
          require('../neon').getDropboxInfo(token_reply.access_token, (err, value) => {
            if (value && value != 'null')
            {
              remote.getGlobal('data').dropbox_connected = true;
              remote.getGlobal('data').dropbox_acc_info = value;
              require('../neon').updateProfile(remote.getGlobal('data').encrypted_user_config_handler, TOML.stringify(remote.getGlobal('data').user_config));

              document.getElementById('status').textContent = 'Connected';
              document.getElementById('status').style.color = 'green';
              document.getElementById('acc_name').textContent = JSON.parse(remote.getGlobal('data').dropbox_acc_info).name.display_name;
              document.getElementById('acc_email').textContent = JSON.parse(remote.getGlobal('data').dropbox_acc_info).email;
              document.getElementById('acc_country').textContent = JSON.parse(remote.getGlobal('data').dropbox_acc_info).country;
              document.getElementById('connect').style.display = 'none';
              disconnectBtn.style.display = 'inline-block';
            }
            else {
              console.log(err);
            }
          });
        }
      });
    }
    catch (e)
    {
      document.getElementById('status').textContent = e;
      document.getElementById('status').style.color = 'red';
    }
  })

  disconnectBtn.addEventListener('click', (event) =>
  {
    remote.getGlobal('data').dropbox_connected = false;
    remote.getGlobal('data').dropbox_acc_info = null;
    remote.getGlobal('data').user_config['dropbox']['token'] = "";
    require('../neon').updateProfile(remote.getGlobal('data').encrypted_user_config_handler, TOML.stringify(remote.getGlobal('data').user_config));

    document.getElementById('status').textContent = 'Not connected';
    document.getElementById('status').style.color = 'red';
    document.getElementById('acc_name').textContent = '';
    document.getElementById('acc_email').textContent = '';
    document.getElementById('acc_country').textContent = '';
    disconnectBtn.style.display = 'none';
    connectBtn.style.display = 'inline-block';
  })
})
